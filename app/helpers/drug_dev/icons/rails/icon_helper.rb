module DrugDev
  module Icons
    module Rails
      module IconHelper
        # Creates an icon tag given an icon name
        #
        # Examples
        #
        #   ddi_icon('calendar')
        #   # => <i class="ddi-calendar"></i>
        def ddi_icon(name, options = {})
          classes = ["ddi-#{name}"] + Array(options.delete(:class))
          content_tag(:i, nil, options.merge(class: classes))
        end
      end
    end
  end
end
