require 'drug_dev/icons/version'
require 'drug_dev/icons/engine' if defined?(::Rails)
require 'json'

module DrugDev
  module Icons
    CONFIG = File.expand_path('../../../icons.json', __FILE__).freeze

    def self.list
      @list ||= JSON.parse(File.read(CONFIG))['icons'].map { |icon| icon['properties']['name'] }
    end
  end
end
