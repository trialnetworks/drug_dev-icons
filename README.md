# DrugDev::Icons

Used for development within DrugDev for consistency in styles. Use the `ddi_icon` helper to generate an `<i>` tag with the correct class, or use the `ddi-*` classes directly. Get a list of the available icons with `DrugDev::Icons.list`.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'drugdev-icons'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install drugdev-icons

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

