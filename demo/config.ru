lib = File.expand_path('../../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'bundler/setup'
require 'drug_dev/icons'

require 'action_view'
require 'sprockets'

class Demo
  def call(env)
    request = Rack::Request.new(env)

    if request.path_info == '/'
      [200, { 'Content-Type' => 'text/html' }, [demo_page]]
    else
      [404, { 'Content-Type' => 'text/plain' }, []]
    end
  end

  def env
    @env ||= begin
      env = Sprockets::Environment.new

      # append paths
      env.append_path File.expand_path('../../app/assets/stylesheets', __FILE__)
      env.append_path File.expand_path('../../app/assets/fonts', __FILE__)

      # add in asset urls
      env.context_class.class_eval do
        include ActionView::Helpers::AssetUrlHelper
      end

      env
    end
  end

  private

    def demo_page
      ERB.new(File.read('demo.html.erb')).result(binding)
    end
end

demo = Demo.new
map('/assets') { run demo.env }
map('/fonts') { run demo.env }
run Demo.new
