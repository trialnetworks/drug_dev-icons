require 'test_helper'

class IconsTest < Minitest::Test

  def test_config
    refute_nil DrugDev::Icons::CONFIG
  end

  def test_list
    list = DrugDev::Icons.list
    assert_kind_of Array, list

    list.each do |icon|
      assert_kind_of String, icon
    end
  end
end
