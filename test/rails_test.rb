require 'test_helper'

class RailsTest < ActionDispatch::IntegrationTest

  # clear the sprockets cache
  teardown do
    FileUtils.rm_rf(File.expand_path('../dummy/tmp', __FILE__))
  end

  def test_engine_loaded
    assert_equal ::Rails::Engine, DrugDev::Icons::Rails::Engine.superclass
  end

  def test_fonts_are_served
    %w[eot woff ttf svg].each do |format|
      get "/assets/DrugDev-icons.#{format}"
      assert_response :success
    end
  end

  def test_stylesheets_served
    get '/assets/drugdev-icons.css'
    assert_font_set(response)
  end

  def test_stylesheets_contain_asset_pipeline_references
    get '/assets/drugdev-icons.css'
    %w[eot eot#iefix woff ttf svg#DrugDev-icons].each do |format|
      assert_match "/assets/DrugDev-icons.#{format}", response.body
    end
  end

  def test_sprockets_require_stylesheet
    get '/assets/sprockets-require.css'
    assert_font_set(response)
  end

  def test_sass_import_stylesheet
    get '/assets/sass-import.css'
    assert_font_set(response)
  end

  def test_scss_import_stylesheet
    get '/assets/scss-import.css'
    assert_font_set(response)
  end

  def test_helpers_available_in_view
    get '/icons'
    assert_response :success
    assert_select 'i.ddi-calendar'
    assert_select 'i.ddi-menu.test'
  end

  private

    def assert_font_set(response)
      assert_response :success
      assert_match(/font-family:\s*'DrugDev-icons';/, response.body)
    end
end
