require 'test_helper'

class DrugDev::Icons::Rails::IconHelperTest < ActionView::TestCase

  def test_builds_icons
    assert_icon i('add'), 'add'
    assert_icon i('add-note'), 'add-note'
  end

  def test_adds_classes
    assert_icon i('calendar', ['test']), 'calendar', class: 'test'
    assert_icon i('charts', ['test2', 'test3']), 'charts', class: 'test2 test3'
    assert_icon i('checkmark', ['test4', 'test5']), 'checkmark', class: ['test4', 'test5']
  end

  def test_passes_options_through
    assert_icon %(<i data-id="42" class="ddi-close"></i>), 'close', data: { id: 42 }
  end

  private

    def assert_icon(expected, *args)
      assert_dom_equal expected, ddi_icon(*args)
    end

    def i(name, classes = [])
      classes.unshift "ddi-#{name}"
      %(<i class="#{classes.join(' ')}"></i>)
    end
end
