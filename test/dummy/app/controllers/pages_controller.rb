class PagesController < ActionController::Base

  # GET /icons
  def icons
    @icons = DrugDev::Icons.list
  end
end
