lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'drug_dev/icons/version'

Gem::Specification.new do |spec|
  spec.name          = 'drug_dev-icons'
  spec.version       = DrugDev::Icons::VERSION
  spec.authors       = ['Kevin Deisz']
  spec.email         = ['info@trialnetworks.com']

  spec.summary       = 'DrugDev icon management'
  spec.description   = 'Stores and manages the icons used by DrugDev apps'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |file| file.match(%r{^test/}) }
  spec.require_paths = ['lib']

  spec.add_dependency 'json'
  spec.add_dependency 'railties', '>= 3.2'

  spec.add_development_dependency 'activesupport'
  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'minitest'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'sass-rails'
end
